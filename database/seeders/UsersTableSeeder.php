<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => env('ADMIN_USERNAME','Admin'),
            'email' => env('ADMIN_EMAIL','admin@example.com'),
            'password' => Hash::make(env('ADMIN_PASSWORD','password')),
        ]);
    }
}
