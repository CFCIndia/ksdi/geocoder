<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>KSDI - Geocoder</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
    <link rel="stylesheet" href="{{url('css/atom-one-light.min.css')}}">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="icon" type="image/png" href="{{url('images/logo.png')}}">
</head>
<body>
  <div id="container">
    <div id="side-pane">
      <div class="tab">
        <button id="btn-geocode" class="tablinks" onclick="openTab(event, 'geocode')">Geocode<br/>Lookup</button>
        <button id="btn-reverse" class="tablinks" onclick="openTab(event, 'reverse')">Reverse<br/>Geocode</button>
      </div>
      <div id="side-pane__geocode" class="tabcontent">
        <h3>Geocode</h3>
        <div><pre><code class="language-json" id="geocoder-results">Please use the search box to see result</code></pre></div>
        <div class="loggedIn">
            <div class="footer">
                <!-- The actual snackbar -->
                <div id="snackbar">Some text some message..</div> 
                <a href="{{ url('sample.csv') }}">Template file for batch Geocode</a><br/><br/>
                <input type="file" id="myFile" name="geocode">
                <button id="upload-button" type="button" onclick="fileUpload()" class="btn btn-filled">Upload new file</button>
                <div id="progress" class="loader" style="display: none;">Loading...</div>
                <a id="download-link" href="javascript:void(0)" download="geocoded-data.csv" class="button btn btn-filled" style="display: none;">Download processed file</a>
                <br/><br/>
                <button type="button" onclick="attemptLogout()" class="btn">Logout</button>
                <p>copyright © 2022 KSDI, Kerala State IT Mission</p>
            </div>
        </div>
        <div class="loggedOut">
            <div class="footer">
                <button type="button" onclick="login()" class="btn">Login</button>
                <p>copyright © 2022 KSDI, Kerala State IT Mission</p>
            </div>
        </div>
      </div>
      <div id="side-pane__reverse" class="tabcontent">
        <h3>Reverse</h3>
        <div><pre><code class="language-json" id="reverse-results">Please use the search box to see result</code></pre></div>
        <div class="loggedIn">
            <div class="footer">
                <button type="button" onclick="attemptLogout()" class="btn">Logout</button>
                <p>copyright © 2022 KSDI, Kerala State IT Mission</p>
            </div>
        </div>
        <div class="loggedOut">
            <div class="footer">
                <button type="button" onclick="login()" class="btn">Login</button>
                <p>copyright © 2022 KSDI, Kerala State IT Mission</p>
            </div>
        </div>
      </div>
    </div>
    <div id="map"></div>
    </div>
    <div id="site-loader">
        <div id="site-loader__content">
            <img src="{{url('images/loading.gif')}}" alt="loading icon" />
        </div>
    </div>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="{{url('js/leaflet.photon.js')}}"></script>
    <script src="{{url('js/ajax-jquery.js')}}"></script>
    <script src="{{url('js/highlight.min.js')}}"></script>
    <script src="{{url('js/app.js')}}"></script>
    <script type="text/javascript">
    function login(){
        window.location = "{{url('/login')}}";
    }
    $(document).ready(function (){
        if(sessionStorage.getItem('api_key')){
            $('.loggedOut').css("display","none");
            $('.loggedIn').css("display","block");
            if(sessionStorage.getItem('id')){
                var id = sessionStorage.getItem('id');
                getJobStatus(id);
            }
        }
    });
    function fileUpload() {
        document.getElementById("myFile").click();
    };

    function attemptLogout() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.responseType = 'json';

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status == 200) {
                var response = xmlhttp.response;
                console.log(response);
                if(response.success){
                    sessionStorage.clear();
                    window.location = "{{url('/')}}";
                } else {
                    message = 'Logout failed';
                    toastMessage(message);
                }
            }
            else if (xmlhttp.status == 401) {
                message = 'Already logged out. Please refresh and login';
                toastMessage(message);
                window.location = "{{url('/login')}}";
            }
            else {
                message = 'Some Error occured! Please try again.';
                toastMessage(message);
            }
            }
        };
        xmlhttp.open("POST", "{{route('logout')}}", true);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        api_key = sessionStorage.getItem('api_key');
        xmlhttp.setRequestHeader('Authorization', 'Bearer ' + api_key);
        xmlhttp.send();
    }

    document.getElementById('myFile').addEventListener('change', function(e) {
        var api_key = sessionStorage.getItem('api_key');
        var file = this.files[0];
        var inputData = new FormData();
        inputData.append("geocode", file);
        e.preventDefault();
        $.ajax({
            url: "{{ route('upload') }}",
            type: 'POST',
            data: inputData,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer '+ api_key);
                $('#site-loader').css("display","block");
            },
            success: function( response ) {
                $('#site-loader').css("display","none");
                if ( response.success === true) {
                    sessionStorage.setItem('id', response.data);
                    toastMessage(response.message);
                    $('#upload-button').attr("disabled", true);
                    $('#download-link').css("display", "none");
                    $('#progress').css("display", "inline-flex");
                    getJobStatus(response.data);
                } else if ( response.success === false) {
                    toastMessage(response.message);
                }
            },
            error: function( data ) {
                $('#site-loader').css("display","none");
                toastMessage('ERROR occured. Check console for detail and contact administrator.');
                console.log(data);
                if(data.status == 401) {
                    message = 'Already logged out. Please refresh and login';
                    toastMessage(message);
                    window.location = "{{url('/login')}}";
                }
            }
        });
        return false;
    });

    function getJobStatus(id){
        var api_key = sessionStorage.getItem('api_key');
        $.ajax({
            url: "{{ route('job_status') }}",
            type: 'GET',
            data: { 'id': id },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer '+ api_key);
            },
            success: function( response ) {
                if ( response.success === true) {
                    if(response.message == 'completed'){
                        $('#upload-button').attr("disabled", false);
                        $('#progress').css("display", "none");
                        $('#download-link').css("display", "inline-flex").attr('href', "{{ url('/') }}/storage/output/" + id + '.csv');
                    }
                    else if(response.message == 'added' || response.message == 'processing') {
                        $('#upload-button').attr("disabled", true);
                        $('#progress').css("display", "inline-flex");
                        setTimeout(function() { getJobStatus(id); }, 5000);
                    }
                    else if(response.message == 'failed'){
                        $('#upload-button').attr("disabled", false);
                        $('#progress').css("display", "none");
                        response.message += '. Try again';
                    }
                    toastMessage('File process status: '+response.message);
                } else if ( response.success === false) {
                    toastMessage(response);
                }
            },
            error: function( data ) {
                toastMessage('ERROR occured. Check console for detail and contact administrator.');
                console.log(data);
                if(data.status == 401) {
                    message = 'Already logged out. Please refresh and login';
                    toastMessage(message);
                    window.location = "{{url('/login')}}";
                }
            }
        });
    }

    function toastMessage(message) {
        // Get the snackbar DIV
        var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        x.innerHTML = message;
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    </script>
  </body>
</html>
