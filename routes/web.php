<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/login', 'LoginController@index');

$router->get('/', [ 'as' => 'home', function () use ($router) {
    return view('home');
}]);

$router->group(['prefix' => 'app/'], function ($router) {
    $router->post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);
    $router->post('/upload', ['as' => 'upload', 'uses' => 'AdminController@batchGeocode']);
    $router->get('/job_status', ['as' => 'job_status', 'uses' => 'AdminController@getJobStatus']);
    $router->post('/logout', ['as' => 'logout', 'uses' => 'AdminController@logout']);
});