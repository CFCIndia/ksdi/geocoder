# Geocoder

Geocoder

## Installation

1. `git clone https://gitlab.com/CFCIndia/ksdi/geocoder.git`
2. Append docker host IP (default: 172.17.0.1) to `listen_addresses` variable in postgresql.conf as follows
```
listen_addresses = 'localhost,172.17.0.1'
```
3. Update IPv4 connection detail in postgres config file pg_hba.conf as follows
```
# IPv4 local connections:
host    all             all             all                     md5
```
4. Check `docker-compose.yml` file and update if any changes required
5. `docker-compose build`
6. `docker-compose up -d`